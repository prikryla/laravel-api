<?php

use App\Http\Controllers\CompanyController;
use App\Http\Controllers\LinkController;
use App\Http\Controllers\SpecialController;
use App\Http\Controllers\WorkController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('{class}/get/{parameter}/{value}', function ($class, $parameter, $value) {
    $policyName = 'App\Models\Policies\\'.$class.'Policy';
    $policy = new $policyName;

    if(!Config::get("admin")){
        if(!in_array('get', $policy->getAllowedCruds()))
            return response("Authorization failed", 401);

        if(!$policy->getAuth($parameter, $value))
            return response("Authorization failed", 401);
    }

    return ("App\\Models\\".$class)::where($parameter, '=', $value)->get();
});

Route::put('{class}', function ($class) {
    $policyName = 'App\Models\Policies\\'.$class.'Policy';
    $policy = new $policyName;
    $data = json_decode(str_replace('data=', '', file_get_contents('php://input')), true);

    if(!Config::get("admin")){
        if(!in_array('add', $policy->getAllowedCruds()))
            return response("Authorization failed", 401);

        if(!$policy->addAuth($data))
            return response("Authorization failed", 401);
    }

    return ("App\\Models\\".$class)::create($data);
});


Route::post('{class}/{id}/edit', function ($class, $id) {
    $policyName = 'App\Models\Policies\\'.$class.'Policy';
    $policy = new $policyName;

    if(!Config::get("admin")){
        if(!in_array('edit', $policy->getAllowedCruds()))
            return response("Authorization failed", 401);

        if(!$policy->editAuth($id, json_decode($_POST["data"], true)))
            return response("Authorization failed", 401);
    }

    $obj = ("App\\Models\\".$class)::where('id', '=', $id)->first();

    foreach(json_decode($_POST["data"]) as $parameter => $value)
        $obj->$parameter = $value;

    $obj->save();
});

Route::delete('{class}/{id}', function ($class, $id) {
    $policyName = 'App\Models\Policies\\'.$class.'Policy';
    $policy = new $policyName;
    $obj = ("App\\Models\\".$class)::where('id', '=', $id)->first();

    if(!Config::get("admin")){
        if(!in_array('delete', $policy->getAllowedCruds()))
            return response("Authorization failed", 401);

        if(!$policy->deleteAuth($obj))
            return response("Authorization failed", 401);
    }
    $obj->delete();
});

Route::post('{class}/getBySQL', function ($class) {
    if(!Config::get("admin"))
        return response("Authorization failed", 401);

    $sql = json_decode($_POST["data"])->sql;
    return ("App\\Models\\".$class)::whereRaw($sql)->get();
});


Route::get('/', function () {
    return view('welcome');
});

Route::group(['prefix' => 'Company/'], function () {
    Route::post('login', [CompanyController::class, 'postLogin']);
    Route::get('{email}/restorePassword', [CompanyController::class, 'getRestorePassword']);
});

Route::group(['prefix' => 'Work/'], function () {
    Route::get('{page}', [WorkController::class, 'getPages']);
    Route::post('add', [WorkController::class, 'postNewWork']);
});

Route::group(['prefix' => 'Link/'], function () {
    Route::get('get/{page}', [LinkController::class, 'getLinks']);
    Route::get('generateReport/{year}/{month}', [LinkController::class, 'generateReport']);
});

Route::group(['prefix' => 'Special/'], function () {
    Route::get('getDashboardData', [SpecialController::class, 'getDashboardData']);
});
