<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Link generated PDF export</title>
</head>
<body>

<table border = "1">
    <tr>
       <th>Category</th>
       <th>Count</th>
    </tr>
    @foreach($report as $rep)
    <tr>
        <td>{{ $rep->type }}</td>
        <td>{{ $rep->count }}</td>
    </tr>
    @endforeach
</table>

</body>
</html>
