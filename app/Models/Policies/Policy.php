<?php

namespace App\Models\Policies;

class Policy
{
    protected $userID;

    function __construct()
    {
        $this->userID = explode(';', app('Illuminate\Http\Request')->header("Authorization"))[0];
    }

    function getAllowedCruds(){
        return $this->allowedCruds;
    }

    function getUserID(){
        return $this->userID;
    }

}
