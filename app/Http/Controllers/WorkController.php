<?php

namespace App\Http\Controllers;

use App\Models\Policies\WorkPolicy;
use App\Models\Work;

class WorkController extends Controller
{
    private $workPolicy;

    public function __construct()
    {
        $this->workPolicy = new WorkPolicy();
    }

    public function postNewWork() {
        $data = json_decode($_POST["data"]);

        $work = Work::create([
            'name' => $data->name,
            'company_id' => $data->company_id,
            'minEpisode' => $data->minEpisode,
            'maxEpisode' => $data->maxEpisode
        ]);

        if (Work::where('name', $data->name)->exists()) {
            return response('Work with this name is already in database!');
        } else {
            $work->save();
            return response('New work has been created.',200);
        }
    }

    public function getPages($page) {
        return Work::where('company_id', $this->workPolicy->getUserID())->offset(0)->limit((int)$page * 20)->get();
    }
}
