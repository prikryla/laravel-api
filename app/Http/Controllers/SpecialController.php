<?php

namespace App\Http\Controllers;

use App\Models\Special;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class SpecialController extends Controller
{
    public function getDashboardData(){
        return Special::select('type', DB::raw('COUNT(type) as count'))
            ->where('date', ">", Carbon::now()->subMonths(10))
            ->get();
    }
}
