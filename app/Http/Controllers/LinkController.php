<?php

namespace App\Http\Controllers;

use App\Models\Link;
use Illuminate\Support\Facades\DB;

use Barryvdh\DomPDF\Facade\Pdf;

class LinkController extends Controller
{
    public function getLinks($page) {
        $data = json_decode($_POST["data"]);
        return Link::where("type", $data->type)
            ->whereBetween("date BETWEEN " . "$data->startDate" . " AND " .  "$data->endDate")
            ->offset(0)
            ->limit($page * 20)
            ->get();
    }

    public function generateReport($year, $month) {
        $report = Link::select('type', DB::raw('COUNT(type) as count'))
            ->whereYear('date', '=', $year)
            ->whereMonth('date', '=', $month)
            ->distinct()
            ->groupBy('type')
            ->get();

        $pdfReport = Pdf::loadView('generateReport', ['report' => $report]);
        return $pdfReport->download('LinkReport.pdf');
    }
}
