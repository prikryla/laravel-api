<?php

namespace App\Http\Controllers;

use App\Models\Company;
use Illuminate\Auth\Events\PasswordReset;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class CompanyController extends Controller
{
    public function postLogin($email, $password) {
        $company = Company::where("email", $email)->first();

        if(!password_verify($password, $company->password))
            return "false";

        $company->token = Str::random(20);
        $company->save();

        return (object)["id" => $company->id, "token" => $company->token];
    }

    public function getRestorePassword($email){

        $response = Password::sendResetLink($email);

        if ($response == Password::RESET_LINK_SENT) {
            $message = "Mail send successfully";
        } else {
            $message = "Email could not be sent to this email address";
        }

        $this->sendResetResponse();

        $response = ['data' => '', 'message' => $message];
        return response($response, 200);
    }

    public function sendResetResponse(){
        $data = json_decode($_POST["data"]);
        $input = $data->only('email','token', 'password', 'password_confirmation');
        $validator = Validator::make($input, [
            'token' => 'required',
            'email' => 'required|email',
            'password' => 'required|confirmed|min:8',
        ]);
        if ($validator->fails()) {
            return response(['errors'=>$validator->errors()->all()], 422);
        }
        $response = Password::reset($input, function ($user, $password) {
            $user->forceFill([
                'password' => Hash::make($password)
            ])->save();

            event(new PasswordReset($user));
        });
        if($response == Password::PASSWORD_RESET){
            $message = "Password reset successfully";
        }else{
            $message = "Email could not be sent to this email address";
        }
        $response = ['data'=>'','message' => $message];
        return response()->json($response);
    }

}
